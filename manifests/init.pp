# technical_lab
#
# A description of what this class does
#
# @summary A short summary of the purpose of this class
#
# @example
#   include technical_lab
class technical_lab {
  include technical_lab::python
  include technical_lab::wsgi

  Class['technical_lab::wsgi']
  -> Class['technical_lab::python']
}
