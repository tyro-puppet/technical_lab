# technical_lab::python
#
# A description of what this class does
#
# @summary A short summary of the purpose of this class
#
# @example
#   include technical_lab::python
class technical_lab::python(
  $app                = 'default',
  $path               = '/var/www/python',
  $additional_folders = ['lib', 'bin', 'data']
  $debug              = false,
) {
  file { $path:
    ensure => 'directory',
    recurse => true,
    source => "puppet:///${module_name}/apps/${app}"
  }

  for $dir in $additional_folders {
    file { '${path}/${dir}':
      ensure: 'directory'
    }
  }

}
