# technical_lab::wsgi
#
# A description of what this class does
#
# @summary A short summary of the purpose of this class
#
# @example
#   include technical_lab::wsgi
class technical_lab::wsgi(
  $url,
  $port    = 80,
  $docroot = '/var/www/python',
  $script  = '/var/www/python/app.wsgi',
  $procs   = 2,
  $threads = 15,
) {

  include apache

  apache::vhost { $url:
    port                        => $port,
    docroot                     => $docroot,
    wsgi_application_group      => '%{GLOBAL}',
    wsgi_daemon_process         => 'wsgi',
    wsgi_daemon_process_options => {
      processes    => $procs,
      threads      => $threads,
      display-name => '%{GROUP}',
    },
    wsgi_import_script          => $script,
    wsgi_import_script_options  => {
      process-group     => 'wsgi',
      application-group => '%{GLOBAL}',
    },
    wsgi_process_group          => 'wsgi',
    wsgi_script_aliases         => {
      '/' => $script,
    },
  }
}
