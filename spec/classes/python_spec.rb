require 'spec_helper'

describe 'technical_lab::python' do
  on_supported_os(facterversion: '2.4').each do |os, os_facts|
    context "on #{os}" do
      let(:facts) { os_facts }

      let(:params) {{
          :app   => 'default1',
          :debug => true,
      }}

      it { is_expected.to compile }

      it { is_expected.to contain_file('/var/www/py').with(
              :ensure => 'directory',
              :source => 'puppet:///technical_lab/apps/default1'
          )
      }

      it { is_expected.to contain_file('/var/www/py/lib').with_ensure('directory') }
      it { is_expected.to contain_file('/var/www/py/bin').with_ensure('directory') }
      it { is_expected.to contain_file('/var/www/py/data').with_ensure('directory') }
    end
  end
end
