require 'spec_helper'

describe 'technical_lab::wsgi' do
  on_supported_os(facterversion: '2.4').each do |os, os_facts|
    context "on #{os}" do
      let(:facts) { os_facts }

      let(:params) {{
        'url' => 'someurl.domain.com',
      }}

      it { is_expected.to compile }
    end
  end
end
